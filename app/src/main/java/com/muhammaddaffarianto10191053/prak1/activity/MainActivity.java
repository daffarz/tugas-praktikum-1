package com.muhammaddaffarianto10191053.prak1.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.muhammaddaffarianto10191053.prak1.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}